﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Universidad.Models
{
    public partial class Alumno
    {
        public Alumno()
        {
            Matricula = new HashSet<Matricula>();
        }

        public int Idalumno { get; set; }
        [StringLength(100), Required]
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public DateTime? FechaNacimiento { get; set; }

        public ICollection<Matricula> Matricula { get; set; }
    }
}
