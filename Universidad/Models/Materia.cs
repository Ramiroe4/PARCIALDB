﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Universidad.Models
{
    public partial class Materia
    {
        public Materia()
        {
            Matricula = new HashSet<Matricula>();
        }

        public int Idmateria { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Display]
        public int? Estado { get; set; }
        public int Precio { get; set; }

        public ICollection<Matricula> Matricula { get; set; }
    }
}
