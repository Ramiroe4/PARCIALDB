﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Universidad.Models
{
    public partial class Profesor
    {
        public Profesor()
        {
            Matricula = new HashSet<Matricula>();
        }

        public int Idprofesor { get; set; }
        [Required(ErrorMessage ="Dato requerido")]
        public string Nombre { get; set; }
        [StringLength(50)]
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public int? Estado { get; set; }

        public ICollection<Matricula> Matricula { get; set; }
    }
}
